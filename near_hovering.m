% questions
% 1. What trajectory generator we could use for p, p_dot, p_dot_dot and psi?
% 2. How we can get estimated values? (p_b, p_b_dot, eta, omega) 
% 3. What values we should accept for PID regulator and another gains?
% 4. How to get closed-loop gain (K) in linear_filter_position? Where should be poles of the system?
% 5. How to get eta_dot? We have only psi_dot..
% 6. Why eta_dot_dot = K_eta_dot*e_eta_dot + K_eta*e_eta ? (Control of MAVs - Part 1 slide 20/26)
% 7. Why on the slide 24/26 we have T, torques des_roll and des_pitch as output wrom NH_contr, when one slide befor output from NH_contr is thrust, des_pith, des_roll, and psi_d_dot?

% Note
% eta=[phi; theta; psi]


% we have in each step
% from estimator
p_b = [0 0 0; 0 0 0.1; 0 0 0.2; 0 0 0.3; 0 0 0.4; 0 0 0.5; 0 0 0.6; 0 0 0.7; 0 0 0.8; 0 0 0.9]';
p_b_dot = [0 0 10; 0 0 10; 0 0 10; 0 0 10; 0 0 10; 0 0 10; 0 0 10; 0 0 10; 0 0 10; 0 0 10]';

eta = [0 0 0; 0 0 0; 0 0 0; 0 0 0; 0 0 0;0 0 0; 0 0 0; 0 0 0; 0 0 0; 0 0 0]';
omega = [0 0 0; 0 0 0; 0 0 0; 0 0 0; 0 0 0;0 0 0; 0 0 0; 0 0 0; 0 0 0; 0 0 0]';

% from trajectory generator 
p_b_d = [0 0 0; 0 0 0.1; 0 0 0.2; 0 0 0.3; 0 0 0.4; 0 0 0.5; 0 0 0.6; 0 0 0.7; 0 0 0.8; 0 0 0.9]';
p_b_d_dot;
p_b_d_dot_dot;
% p_b_d_dot_dot_dot; - not used in near_hovering
psi_d = [0 0 0 0 0 0 0 0 0 0];


% parameters
m_b = 1;
g = 9.81;
J_b = [0.007 0 0; 0 0.007 0; 0 0 0.012];
kd = [0.1; 0.1; 0.1];               %for PID; axes [x; y; z] ??????
kp= [2; 2; 2];                      %for PID; axes [x; y; z] ??????
k_psi = 1;                          %???????
K_eta_dot = [1 2 3; 4 5 6; 7 8 9];  %???????
K_eta = [1 2 3; 4 5 6; 7 8 9];      %???????


% time step
dt=0.1;
T=10;

% calculations
for i=0:1:10 % i=0:1:T/dt
  phi = eta(1,i);
  theta = eta(2,i);
  psi = eta(3,i);
  
  u_t(i)= (-m_b/(cos(phi)*cos(theta)))*(-g + p_d_dot_dot(3,i) + kd(3)*(p_b_d_dot(3,i) - p_b_dot(3,i)) + kp(3)*(p_b_d(3,i) - p_b(3,i)))
  
  Q= [cos(phi)*cos(psi) sin(psi); ...
      cos(phi)*sin(psi) -cos(psi)];
  sin_phi_theta_d = - m * inv(Q) / u_t(i) * (p_b_d_dot_dot(1:2,i) + kd(1:2)*(p_b_d_dot(1:2,i) - p_b_dot(1:2,i)) + kp(1:2)*(p_b_d(1:2,i) - p_b(1:2,i)))
  psi_d_dot = k_psi*atan2(sin(psi_d(i) - psi), cos(psi_d - psi));
  
  e_eta = [asin(sin_phi_theta_d(1)); asin(sin_phi_theta_d(2)); psi_d(i)] - eta;
%  e_eta_dot = ?????
%  T_dot = ????? 
  tau = skew(omega(:,i))*J_b*omega(:,i) + J_b*inv(T(phi, theta))*(K_eta_dot*e_eta_dot + K_eta*e_eta - T_dot*omega(:,i)) 
  
end