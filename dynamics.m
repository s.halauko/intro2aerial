% questions:
% 1. What are m_b and J_b for the real drone and what values we should accept for simulations?
% 2. How to integrate numericalu? We don't have limits. I accepted that value in this step is integral from the previous to this step.
% 3. What is initial value of R and omega? 
% 4. R and R_dot is going to zeros, something is wrong. What?
% 5. What is it e_a as the output drom dynamics at the slide 24/26 Control of MAVs - Part 1?


clear all; clc;

%input data to the moodel

%u_t = 1;
%tau = [1;1;1];

u_t = [10 10 10 10 10 10 10 10 10 10];
tau = [0 0 0 0 0 0 0 0 0 0; 0 0 0 0 0 0 0 0 0 0; 0 0 0 0 0 0 0 0 0 0];


%parameters
m_b = 1;
J_b = [0.007 0 0; 0 0.007 0; 0 0 0.012];
g = [0; 0; 9.81];
e3 = [0; 0; 1]; %vector down for R

%initial data
omega_0 = [0; 0; 0];
R_0 = [1 0 0; 0 1 0; 0 0 1];
p_b_dot_dot = [0;0;0];
p_b_dot = [0;0;0];
p_b = [0;0;0];

%time_step and time of simulation
dt=0.01;
T=10;

omega = omega_0
R = R_0

%calculations
for i=1:1:10 %i=1:1:T/dt
  omega_dot(:,i) = inv(J_b) * (-skew(omega(:,i))*J_b*omega(:,i) + tau(:,i))
  omega(:,i+1)=omega_dot(:,i)*dt
  R_dot = R*skew(omega(:,i))
  R=R_dot*dt
  p_b_dot_dot(:,i) = g - R*e3*u_t(i)/m_b
  p_b_dot(:,i) = p_b_dot_dot(:,i)*dt
  p_b(:,i) = p_b_dot(:,i)*dt
end 

